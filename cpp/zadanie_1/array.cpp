#include <iostream>
#include <cstdlib>
#include <array>
#include <algorithm>

using namespace std;

int main(void)
{
    array<int, 100> arr;

    for (int &value : arr)
    {
        value = rand();
    }

    cout << "W array znajduje sie liczby:  " << endl ;

    for (int &value : arr)
    {
        cout << value << " ";
    }
    cout << endl;

    sort(arr.begin(), arr.end());

    cout << "A po sortowaniu: ";
    
    for (int &value : arr)
    {
        cout << value << " ";
    }
    cout << endl;

    return 0;
}
