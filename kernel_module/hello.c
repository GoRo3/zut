#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("GORO3");
MODULE_DESCRIPTION ("Prosty modul Hello World");

static int __init hello_init(void){

    printk(KERN_INFO "Dzien Dobry \n");

    return 0; 
}

static void __exit hello_exit(void){

printk(KERN_INFO "Do widzenia \n");
}

module_init(hello_init);
module_exit(hello_exit);