#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct el
{

    char imie[10];
    char numer_t[12];

    struct el *next;

} LISTA;

LISTA *begin = NULL;

void add_list(char *name, char *mobile);
void show_list(void);
void clean(void); 

int main(void)
{
    add_list("Jan", "+48669890432");
    add_list("Gutenberg", "+48123456789");
    add_list("Helga", "+49300600123"); 

    show_list();

    printf("\n Hit any key to exit");
    getchar();
    clean();
    return 0;
}

void add_list(char *name, char *mobile)
{
    LISTA *temp = (LISTA *)malloc(sizeof(LISTA));
    if (NULL == temp)
    {
        printf("No memory in the system");
        exit(EXIT_FAILURE);
    }
    strcpy(temp->imie, name);
    strcpy(temp->numer_t, mobile);

    temp->next = begin;

    begin = temp;
}

void show_list(void)
{
    LISTA *tmp = begin; 
    uint i = 1; 

    printf("\n\n\t Imie:\t\ttelefon:\n");
    
    while(NULL!=tmp)
    {
        printf("l.p. %d - %10s , %12s \n", i , tmp->imie, tmp->numer_t);
        tmp = tmp->next; 
        i++;
    }
}

void clean(void)
{
    LISTA *tmp = begin; 
    while(NULL!=begin)
    {
        tmp = begin;
        begin = begin->next;
        free(tmp);  
    }

}
