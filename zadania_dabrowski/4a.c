#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define FIFO_DIR "/tmp/fifo1"

int main(void)
{
	int pipe;
	char buff[] = "This is a FIFO example";

	pipe = mkfifo(FIFO_DIR, 0666 );

	while(1)
	{
	pipe = open(FIFO_DIR,O_WRONLY);
	write(pipe,buff,strlen(buff)+1);
	close(pipe);
	sleep(3);
	}

return 0;
}
