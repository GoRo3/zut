#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define FIFO_DIR "/tmp/fifo1"

int main(void)
{
	int pipe;
	char buff[99];
	while(1)
	{
	pipe = open(FIFO_DIR,O_RDONLY);
	read(pipe,buff,sizeof(buff));
	printf("%s \n", buff);
	close(pipe);
	}

return 0;
}
