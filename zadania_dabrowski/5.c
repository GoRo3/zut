//gcc -pthread -o 5 5.c

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

sem_t sem_bin;

void *watek_1(void *arg)
{

	while(1)
	{
	sem_wait(&sem_bin);
	printf("Watek 1: Żyrafy wchodzą do szafy. \n");
	sem_post(&sem_bin);
	sleep(3);
	}

pthread_exit(NULL);
}

void *watek_2(void * arg)
{
	while(1)
	{
	sem_wait(&sem_bin);
	printf("Watek 2: Pawiany wchodza na sciany. \n");
	sem_post(&sem_bin);
	sleep(3);
	}

pthread_exit(NULL);
}

int main(void)
{
pthread_t thread1, thread2;

sem_init(&sem_bin,0,1);

pthread_create(&thread1,NULL,watek_1,NULL);
pthread_create(&thread2,NULL,watek_2,NULL);


pthread_join(thread1,NULL);
pthread_join(thread2,NULL);

return 0;
}
