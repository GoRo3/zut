#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_ADRESS "./liczby.bin"

void sortuj(int tab[], int n);
void zlicz_liczby(char *addres)
{
	int i=0;

	FILE *fd = fopen(addres,"r");

	size_t rozmiar=0;

	fseek(fd,0,SEEK_END);

	rozmiar = ftell(fd)/4;

	fseek(fd,0,SEEK_SET);

	int buff[rozmiar];

	while(fscanf(fd,"%i",&buff[i])!=EOF)
	{
		i++;
	}

	fclose(fd);

	int j=0;

	while(j<rozmiar)
		{
		printf(" %i " ,buff[j]);
		j++;
		}
	j=0;

	printf("\n");

	sortuj(buff,((sizeof(buff)/sizeof(buff[0]))));

	while(j<5)
		{
		printf(" %i ",buff[j]);
		j++;
		}
	printf("\n");

}

int main(void)
{
	char adress[] = FILE_ADRESS;
	zlicz_liczby(adress);

return 0;
}

void sortuj(int tab[], int n)
{

int i,j;

for (i = 0; i < n-1; i++)
	{
	       for (j = 0; j < n-i-1; j++)
		{
       		    if (tab[j] < tab[j+1])
       		       	{
				int temp = tab[j];
    				tab[j] = tab[j+1];
    				tab[j+1] = temp;
			}
		}
	}
}
